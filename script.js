// set globals var
let url = 'https://api.sheety.co/8d7b9bd2c34aeebb6fb59a8fc34298a7/vacs2K20/vacances';
let data = []
let results = []
let listRow = []

// get the data of the sheet
function loadData() {
    document.getElementById('bar').style.width = '100%'
    fetch(url)
    .then((response) => response.json())
    .then(json => {
        data = json.vacances;
        setData();
    })
    .catch(err => {
        console.error(`Erreur du chargement des données : ${err}`)
        document.getElementById('bar').style.width = '0'
    })
}

// reset all variables and the page
function resetData() {
    placeholder()
    data = []
    results = []
    listRow = []
}


// create template
function placeholder() {

    document.getElementById("grid").innerHTML = "";

    for (let index = 0; index < 12; index++) {
        document.getElementById("grid").insertAdjacentHTML("beforeend",
        `
        <div class="col-6 col-sm-4 col-md-3 card-grid">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title placeholder">
                    </h5>
                    <div class="card-image placeholder">
                    </div>
                    <p class="card-text text-primary placeholder" style="font-weight: bold">
                    </p>
                </div>
            </div>
        </div>
        `)
    }
}

// load the page
placeholder()
resetData()
loadData()

// set the refresh button
let myBtn = document.getElementById('button')
myBtn.addEventListener("click", function (){
    resetData();
    loadData();
});

// set data in a readable way
function setData() {
    let jsp = Object.entries(data[0])
    jsp.map(e => {
        listRow.push(e[0])
    })
    countData()
}

function countData() {

    // for each column
    listRow.map((e) => {

        // for each row
        let listData = []
        data.map(oui => {
            listData.push(oui[e])
        })

        // filter undefined entry and numbers
        listData = listData.filter(element => element !== undefined);
        listData = listData.filter(element => typeof element == 'string');

        // capitalize first letter
        listData.map(answer => {
            answer = capitalize(answer)
        })

        // check the question format 
        let question = insertSpaces(e)
        question = question.toLowerCase()

        // push object to results tab
        results.push( {
            id: e,
            title: capitalize(question),
            content: searchMost(listData)
        })
    })

    // delete first and last result
    results.shift()
    results.pop()

    // set up data on the page
    setupData()
}

// seatch the entry with the most occurence in a tab
function searchMost(array)
{
    if(array.length == 0)
        return null;
    var modeMap = {};
    var maxEl = array[0], maxCount = 1;
    for(var i = 0; i < array.length; i++)
    {
        var el = array[i];
        if(modeMap[el] == null)
            modeMap[el] = 1;
        else
            modeMap[el]++;  
        if(modeMap[el] > maxCount)
        {
            maxEl = el;
            maxCount = modeMap[el];
        }
    }
    return maxEl;
}

// set up the datas in the page
function setupData() {
    document.getElementById("grid").innerHTML = "";

    results.forEach(element => {        
        document.getElementById("grid").insertAdjacentHTML("beforeend",
        `
        <div class="col-6 col-sm-4 col-md-3 card-grid">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title" data-id="${element.id}">
                        ${element.title}
                    </h5>
                    <div class="card-image">
                        <img src="images/${element.content}.jpg">
                    </div>
                    <p class="card-text text-primary" style="font-weight: bold">
                        ${element.content}
                    </p>
                </div>
            </div>
        </div>
        `
        )
    })

    // stop loading bar
    document.getElementById('bar').style.width = '0'
}

// capitalize first letter of a string
function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

// inster spaces before all capital letters of a string
function insertSpaces(string) {
    string = string.replace(/([a-z])([A-Z])/g, '$1 $2');
    string = string.replace(/([A-Z])([A-Z][a-z])/g, '$1 $2')
    return string;
}
