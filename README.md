# Vacances 2021

- [x] Récuperer les données
- [x] Mettre en forme les questions
- [ ] Uniformiser les réponses (j'y arrive pas Sadge)
- [ ] Afficher le nom de ceux qui ont mis le bon résultat
- [ ] Ne pas afficher les questions ouvertes (ou afficher toutes les réponses)
- [x] Ajouter un bouton refresh
- [x] Afficher un template avant le chargement
- [ ] Trouver une image pour chaque personne, et geré le fait de pouvoir n'avoir aucune image
- [ ] Gérer le multi réponse